* ECAL automation-control package
  This package provide all the utilities necessary to run the ECAL Automation workflows with the influxdb backend:
  - Update job status to the db
  - Control job progress

* Requirements
  This package works only with python3

* Installation
  #+BEGIN_EXAMPLE
  pip install "git+https://gitlab.cern.ch/cms-ecal-dpg/ECALELFS/automation-control.git"
  #+END_EXAMPLE
  
* Usage
  - [[https://cmsecaldocs.web.cern.ch/cmsecaldocs/ecalautoctrl/][Online documentation]]
  - for the =ecalautomation.py= script you may also refer to the script help:
    =ecalautomation.py --help=

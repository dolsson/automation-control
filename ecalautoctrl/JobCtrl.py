import sys
import enum
from typing import Optional, List, Dict
from copy import deepcopy as dcopy
from datetime import datetime

from influxdb import InfluxDBClient
import urllib3

urllib3.disable_warnings()

from ecalautoctrl.credentials import *

class JobStatus(enum.Enum):
    """
    Simple class to encode job statuses
    """

    idle = (enum.auto(),)
    running = (enum.auto(),)
    failed = (enum.auto(),)
    done = enum.auto()


class JobCtrl:
    """
    Update job status and information to the influxdb.
    Each combination of campaign+era+workflow is called a task.

    The job status is represented by the following table::

       {
           'measurement' : 'job',
           'tags' : {
               'workflow' : 'ECAL offline workflow name',
               'campaign' : 'Campaign to which the job belongs to',
               'id'       : 'Progressive number starting from 0 that identify the current job within the task'
           },
           'time' : timestamp,
           'fields' : {
               'idle' : boolean,
               'running' : boolean,
               'failed' : boolean,
               'done' : boolean
           }
       }

    User defined tags and fields can also be appended to the default list.

    :param workflow: name of the workflow (e.g. ECALELF_prod, ECALELF_ntuples, PHISYM_prod, PHISYM_merge, ...)
    :param campaign: name of the processing campaign.
    :param tags: other user defined tags.
    :param fields: user specified fields.
    :param dbname: database name.
    """

    def __init__(
        self,
        workflow: str = None,
        campaign: str = None,
        tags: Optional[List[Dict]] = None,
        fields: Optional[List[str]] = [],
        dbname: Optional[str] = "ecal_offline_workflows",
    ):
        """
        Create a new JobCtrl workflow
        """

        ### allow only pre-determined workflows
        if workflow is None:
            sys.exit("[JobCtrl::init] The workflow field is mandatory")

        ### require a campaign
        if campaign is None:
            sys.exit("[JobCtrl::init] The campaign field is mandatory")

        ### create point data template
        self.global_data = {
            "measurement": None,
            "tags": {
                "workflow": str(workflow),
                "campaign": str(campaign),
            },
            "time": None,
            "fields": {
                "idle": 0,
                "running": 0,
                "failed": 0,
                "done": 0,
            },
        }
        ### user tags
        for tag, v in tags.items():
            self.global_data["tags"][tag] = v
        ### user fields
        for field, v in fields.items():
            self.global_data["fields"][field] = v

        self.db = InfluxDBClient(
            host=dbhost,
            port=dbport,
            username=dbusr,
            password=dbpwd,
            ssl=dbssl,
            database=dbname,
        )

        ### common tools
        self.match_tags = ""
        for t, v in self.global_data["tags"].items():
            if self.match_tags != "":
                self.match_tags += " AND "
            self.match_tags += "\"{}\" = '{}'".format(t, v)

    def taskExist(self):
        """
        Check if specified task exist already in the db.

        :rtype: bool
        """

        exist = len(
            self.db.query(
                f'SELECT * FROM "task_summary" WHERE {self.match_tags}'
            )
        )

        return exist > 0

    def createTask(self, jids: List = []) -> bool:
        """
        Set newly submitted jobs to idle state. This method should be called by the submit script.

        :param jids: submitted job IDs, defaults to [].
        """

        ### check ids
        if len(jids) < 1:
            sys.exit("[JobCtrl::createTask] Job ids list is empty")

        ### check that a task does not already exist for this workflow+campaign+era combination
        if self.taskExist():
            sys.exit(
                '[JobCtrl::createTask] Workflow self.global_data["tags"]["workflow"] \
                already exist for self.global_data["tags"]["campaign"]/self.global_data["tags"]["era"] campaign/era'
            )

        ### insert jobs with status set to idle
        data = [dcopy(self.global_data) for _ in range(len(jids))]
        subtime = datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ")
        for i, jid in enumerate(jids):
            data[i]["measurement"] = "job"
            data[i]["time"] = subtime
            data[i]["tags"]["id"] = str(jid)
            data[i]["fields"][JobStatus.idle.name] = 1

        # data.append(task_data)
        return self.db.write_points(data)

    def getJob(self, jid: int) -> Dict:
        """
        Retrive from the influx db the information of a single job in the current task.

        :param jid: the job id.
        :return: a dictionary containing the job information.
        """

        jobs_query = self.db.query(
            'SELECT * FROM "job" WHERE %s AND "id"=\'%s\'' % (self.match_tags, str(jid))
        )

        return jobs_query

    def getJobs(self) -> Dict:
        """
        Retrive from the influx db all jobs belonging to the current task

        :return: a dictionary containing the list of job ids in a given status.
        """

        jobs_query = self.db.query(
            'SELECT last("running") AS "running", last("idle") AS "idle", last("failed") AS "failed", last("done") AS "done" FROM "job" WHERE %s GROUP BY "id"'
            % self.match_tags
        )

        jobs = {
            "idle": [],
            "running": [],
            "failed": [],
            "done": [],
        }
        for jid, job in jobs_query.items():
            data = next(job)
            for status, v in jobs.items():
                if data[status] > 0:
                    v.append(jid[1]["id"])

        return jobs

    def setStatus(self, jid: int = None, status: JobStatus = JobStatus.idle) -> bool:
        """
        Set status of job #jid.
        This methods (or its shortcuts) should be called by the job itself.

        :param jid: job id within the submission, defaults to None.
        :param status: job new status, defaults to JobStatus.idle.
        """

        ### check id
        if jid == None:
            sys.exit("[JobCtrl::setStatus] Please specify a vaild job id")

        ### check if job already exist in db (should have been injected by createTask)
        exist = len(
            self.db.query(
                'SELECT * FROM "job" WHERE %s AND "id" = \'%s\''
                % (self.match_tags, str(jid))
            )
        )
        if not exist:
            sys.exit(
                "[JobCtrl::setStatus] Job %s not found in %s+%s+%s task. Please submit the task first using JobCtrl::createTask"
                % (
                    str(jid),
                    self.global_data["tags"]["workflow"],
                    self.global_data["tags"]["campaign"],
                    self.global_data["tags"]["era"],
                )
            )

        ### check status
        if status.name not in JobStatus.__members__.keys():
            sys.exit(
                "[JobCtrl::setStatus] Specified status %s is not valid. Valid statuses are: \n\t%s"
                % (status, "\n\t".join(JobStatus.__members__.keys()))
            )

        data = dcopy(self.global_data)
        data["measurement"] = "job"
        data["time"] = datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ")
        data["tags"]["id"] = str(jid)
        data["fields"][status.name] = 1

        ### check data consistency
        if sum([data["fields"][sts] for sts in JobStatus.__members__.keys()]) != 1:
            sys.exit(
                "[JobCtrl::setStatus] More then one status is being set for the same job (id=%s)"
                % str(jid)
            )

        return self.db.write_points([data])

    def getIdle(self) -> List:
        """
        Get all jobs in the current task in idle state.

        :return: the list of job ids in idle state.
        """

        return self.getJobs()["idle"]

    def getRunning(self) -> List:
        """
        Get all jobs in the current task in running state.

        :return: the list of job ids in running state.
        """

        return self.getJobs()["running"]

    def getFailed(self) -> List:
        """
        Get all jobs in the current task in failed state.

        :return: the list of job ids in failed state.
        """

        return self.getJobs()["failed"]

    def getDone(self) -> List:
        """
        Get all jobs in the current task in done state.

        :return: the list of job ids in done state.
        """

        return self.getJobs()["done"]

    def idle(self, jid: int = None):
        """
        Set job #jid status to idle

        :param jid: job id within the submission, defaults to None.
        """

        self.setStatus(jid=jid, status=JobStatus.idle)

    def running(self, jid: int = None):
        """
        Set job #jid status to running

        :param jid: job id within the submission, defaults to None.
        """

        self.setStatus(jid=jid, status=JobStatus.running)

    def failed(self, jid: int = None):
        """
        Set job #jid status to failed

        :param jid: job id within the submission, defaults to None.
        """

        self.setStatus(jid=jid, status=JobStatus.failed)

    def done(self, jid: int = None):
        """
        Set job #jid status to done

        :param jid: job id within the submission, defaults to None.
        """

        self.setStatus(jid=jid, status=JobStatus.done)

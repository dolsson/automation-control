#!/usr/bin/env python3
import htcondor
import subprocess
from ecalautoctrl import RunCtrl, QueryDBS, JobCtrl

dbInit = RunCtrl("test")
jobCtrl = JobCtrl(workflow = "test", campaign = "test")

runs = dbInit.getRuns(status = ["phisym-new"])

dataQuery = QueryDBS()
files = dataQuery.getRunsFiles([int(run['run_number']) for run in runs])

filesNames = []


            
job = htcondor.Submit({
    "executable" : "python3",
    "arguments" : ["/afs/cern.ch/user/e/ecalgit/online_dev/PhiSymReco_cfg.py", "inputFiles=$(logical_file_name)"],
    "getenv" : True,
    "max_retries" : 3})

schedd = htcondor.Schedd()

submit_results = []
if files is not None:
    for run, files in files.items():
        submit_results.append(schedd.submit(job, itemdata = iter(files)))

print(submit_results)
        
#subprocess.run(["python3", "/afs/cern.ch/user/e/ecalgit/online_dev/PhiSymReco_cfg.py", "inputFiles="+','.join(filesNames)])

#dbInit.updateStatus(runs, {'new': False})


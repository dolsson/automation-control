from ecalautoctrl.credentials import *
from typing import List, Dict, Optional
from datetime import datetime
from dbsClient.apis.dbsClient import DbsApi
from influxdb import InfluxDBClient
import logging
import runregistry
import urllib3
urllib3.disable_warnings()

class QueryRunRegistry:
    """
    Custom interface to CMS run registry.
    """

    def __init__(self):
        pass
<<<<<<< HEAD
        
=======

>>>>>>> f16a144c970e593da6614a29af1cd285f773e635
    def get_runs(self,
                 min_run: int=34000,
                 max_run: Optional[int]=None,
                 min_start: Optional[str]=None,
                 max_start: Optional[str]=None) -> List[Dict]:
        """
        Returns run records from the run registry, filtered by kwargs
        Applies as deafult filters "stable beam" and "ecal included"
<<<<<<< HEAD
=======

        :param min_run: oldest run to be considered
        :param max_run: latest run to be considered
        :param min_start: time window start
        :param max_start: time window stop
        """
>>>>>>> f16a144c970e593da6614a29af1cd285f773e635

        :param min_run: oldest run to be considered
        :param max_run: latest run to be considered
        :param min_start: time window start
        :param max_start: time window stop
        """
        
        ### Standard filters:
        #   stable beam
        #   ecal included in the DAQ
        run_filter = {'stable_beam':{'=':True},'ecal_included':{'=':True}}

        if min_run is not None and max_run is not None:
            run_filter.update({"run_number": {"and": [{">=": min_run}, {"<=": max_run}]}})
        elif min_run is not None:
            run_filter.update({"run_number": {">=": min_run}})
        elif max_run is not None:
            run_filter.update({"run_number": {"<=": max_run}})

        if min_start is not None and max_start is not None:
            run_filter.update({"start_time": {"and": [{">=":(min_start)}, {"<=":(max_start)}]}})
        elif min_start is not None:
            run_filter.update({"start_time": {">=":(min_start)}})
        elif max_start is not None:
            run_filter.update({"start_time": {"<=":(max_start)}})

        result = []
        try:
            # logger.info( 'Calling the run registry api query="%s"', run_filter)

            for row in runregistry.get_runs(filter=run_filter):

                result.append({
                    'run_number': row['run_number'],
                    'startTime':(row['oms_attributes']['start_time']),
                    'stopTime':(row['oms_attributes']['end_time']),
                    #'runClassName': row['class'],
                    'components': ','.join(el for el in row['oms_attributes']['components']),
                    'sequence': row['oms_attributes']['sequence'],
                    'stableBeams': row['oms_attributes']['stable_beam'],
                })

        except Exception as e:
            try:
                print('Error when accessing the run registry %s ' % ( e ))
            except:
                print('Error when accessing the run registry %s' % e)

        return result

class QueryDBS:
    """
    Get dataset/files information from DBS

    :param dataset: dataset name.
    :param dbs_instance: address of the DBS instance to submit queries to.
    """

    def __init__(self, dataset: Optional[str]='/AlCaPhiSym/*/RAW',
                 dbs_instance: Optional[str]='https://cmsweb.cern.ch/dbs/prod/global/DBSReader'):
        self.dataset = dataset
        self.dbs = DbsApi(dbs_instance)
<<<<<<< HEAD

=======
>>>>>>> f16a144c970e593da6614a29af1cd285f773e635
    def getRunFiles(self, run: int) -> list:
        """
        Collect the list of file names for a single run.

        :param run: CMS run number.
        """
        files = []
        data = self.dbs.listDatasets(dataset = self.dataset, run_num = int(run))
        if data != []:
<<<<<<< HEAD
            files = self.dbs.listFiles(dataset = data[-1]["dataset"], run_num = int(run))

=======
            for j in data:
                files.append(self.dbs.listFiles(dataset = j["dataset"], run_num = int(run)))
>>>>>>> f16a144c970e593da6614a29af1cd285f773e635
        return files

    def getRunsFiles(self, runs: List[int]) -> Dict[int, List]:
        """
        Collect the list of file names for each specified run.

        :param runs: list of CMS run numbers.
        """
        files = {}
        for run in runs:
            files[run] = self.getRunFiles(run)

        return files
<<<<<<< HEAD
   
=======

>>>>>>> f16a144c970e593da6614a29af1cd285f773e635
class RunCtrl:
    """
    Update ECAL influxdb instance with new run info.

    :param acq_era: T0 acquisition era.
    """
    def __init__(self, acq_era: str, dbname: str='ecal_online_workflows') -> None:
        self.db = InfluxDBClient(host=dbhost, port=dbport, username=dbusr, password=dbpwd, ssl=dbssl, database=dbname)
        self.era = str(acq_era)
<<<<<<< HEAD
        
=======

>>>>>>> f16a144c970e593da6614a29af1cd285f773e635
    def updateStatus(self, runs: list, status: Dict[str, bool]={'new': True}) -> bool:
        """
        Write data to influxdb.

        :param runs: list of new runs.
        :param status: status map.
        """
        data = []
        for rn in runs:
            data.append({
                'measurement' : 'run',
                'tags' : { 'run_number' : rn, 'era' : self.era },
                'fields' : dict(status) ,
                'time' : datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ")
            })
        ret = self.db.write_points(data)

<<<<<<< HEAD
        return ret    

    def getWorkflows(self) -> Dict:
        """
        Returns the list of workflows that are part of the automation
        """

        data = self.reformat_last(self.db.query('SELECT last(*) FROM "workflows"'))[-1]
        
        return {
            'active' : data['active'].split(','),
            'inactive' : data['inactive'].split(',')
        }    
    
=======
        return ret

>>>>>>> f16a144c970e593da6614a29af1cd285f773e635
    def getRuns(self, status: Optional[List]=[], selection: Optional[str]=None) -> List:
        """
        Retrieve runs from the ECAL db satisfying certain conditions.

        :param status: list of statuses the run has reached (if more than one AND is assumed).
        :param selection: alternatively specify a custom query string.
        """

        if len(status)==0 and not selection:
            logging.warning("Both status list and selection are empty")
            return []
<<<<<<< HEAD
        
=======

>>>>>>> f16a144c970e593da6614a29af1cd285f773e635
        if not selection:
            selection = ' AND '.join(['"last_'+s+'" = true' for s in status])

        data = self.db.query(f'SELECT * FROM (SELECT last(*) FROM "run" GROUP BY run_number) WHERE {selection}')

        return self.reformat_last(data)

    def getLastRun(self) -> int:
        """
        Retrieve last run. This is the most recent run acquired in CMS and injected
        in the automation workflow (i.e. the largest run number). This is not necessarly
        the last run injected chronologically into the system.
        """

        runs = self.reformat_last(self.db.query('SELECT * FROM (SELECT last(*) FROM "run" GROUP BY run_number)'))

        return int(max(runs, key=lambda r : r['run_number'])['run_number'])
<<<<<<< HEAD
    
=======

>>>>>>> f16a144c970e593da6614a29af1cd285f773e635
    def reformat_last(self, data : Dict) -> Dict:
        """
        Remove the automatically prepended 'last_' to results from queries involving last()

        :param data: data returned by InfluxDBClient.query
        """
<<<<<<< HEAD
    
=======

>>>>>>> f16a144c970e593da6614a29af1cd285f773e635
        # copy keys to avoid 'dictionary changed during loop' error
        rdata = []
        if len(data):
            for d in data.items()[-1][-1]:
                keys = list(d.keys())
                for key in keys:
                    d[key.replace('last_', '')] = d.pop(key)
                rdata.append(d)
<<<<<<< HEAD
                
        return rdata

            
=======

        return rdata
>>>>>>> f16a144c970e593da6614a29af1cd285f773e635

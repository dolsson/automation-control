import runregistry
import requests
from typing import List, Dict, Optional
from dbsClient.apis.dbsClient import *
from influxdb import InfluxDBClient
from typing import List, Dict, Optional
import urllib3
urllib3.disable_warnings()

from ecalautoctrl.credentials import *

class QueryRunRegistry:
    """
    Custom interface to CMS run registry.
    
    :param min_run: oldest run to be considered
    :param max_run: latest run to be considered
    :param min_start: time window start
    :param max_start: time window stop
    """
    
    def __init__(self,
                 min_run: int=34000,
                 max_run: Optional[int]=None,
                 min_start: Optional[str]=None,
                 max_start: Optional[str]=None) -> None:
        
        self.min_run = min_run
        self.max_run = max_run
        self.min_start = min_start
        self.max_start = max_start
        
    def get_runs(self) -> List[Dict]:
        """
        Returns run records from the run registry, filtered by kwargs
        Applies as deafult filters "stable beam" and "ecal included"
        """
        min_run = self.min_run
        max_run = self.max_run
        min_start = self.min_start
        max_start = self.max_start

        ### Standard filters:
        #   stable beam
        #   ecal included in the DAQ
        filter = {'stable_beam':{'=':True},'ecal_included':{'=':True}}

        if min_run is not None and max_run is not None:
            filter.update({"run_number": {"and": [{">=": min_run}, {"<=": max_run}]}})
        elif min_run is not None:
            filter.update({"run_number": {">=": min_run}})
        elif max_run is not None:
            filter.update({"run_number": {"<=": max_run}})

        if min_start is not None and max_start is not None:
            filter.update({"start_time": {"and": [{">=":(min_start)}, {"<=":(max_start)}]}})
        elif min_start is not None:
            filter.update({"start_time": {">=":(min_start)}})
        elif max_start is not None:
            filter.update({"start_time": {"<=":(max_start)}})

        print("Retrieving attributes from " + str(min_run) + " to " + str(max_run))

        try:
            # logger.info( 'Calling the run registry api query="%s"', filter)
            result = []

            for row in runregistry.get_runs(filter=filter):
                        
                result.append({
                    'number': row['run_number'],
                    'startTime':(row['oms_attributes']['start_time']),
                    'stopTime':(row['oms_attributes']['end_time']),
                    #'runClassName': row['class'],
                    'components': ','.join(el for el in row['oms_attributes']['components']),
                    'stableBeams': row['oms_attributes']['stable_beam'],
                    'sequence': row['oms_attributes']['sequence'],
                })
            
            return result

        except Exception as e:
            try:
                msg = ': ' + e.readlines()
                print('Error when accessing the run registry %s %s' % ( e, msg ))
            except:
                print('Error when accessing the run registry %s' % e)


class QueryDBS:
    """
    Get dataset/files information from DBS

    :param runs: list of runs for which data files are requested.
    :param dataset: dataset name.
    """
    
    def __init__(self, runs: list, dataset: Optional[str]='/AlCaPhiSym/*/RAW') -> None:
        self.runs = runs
        self.dataset = dataset
        self.dbs = DbsApi('https://cmsweb.cern.ch/dbs/prod/global/DBSReader')
        
    def fetchFiles(self) -> list:
        """
        Collect the list of file names.
        """
        files = []
        for i in self.runs:
            data = self.dbs.listDatasets(dataset = self.dataset,run_num = i['number'])
            if data != []:
                for j in data:
                    files.append(self.dbs.listFiles(dataset = j["dataset"],run_num = i['number']))
        return files

class DatabaseWrite:
    """
    Update ECAL influxdb instance with new run info.

    :param fetchedRuns: list of new runs.
    :param files: list of data files.
    """
    def __init__(self, fetchedRuns: list, files: Optional[List]=[]) -> None:  
        self.fetchedRuns = fetchedRuns
        self.files = files
        self.db = InfluxDBClient(host=dbhost, port=dbport, username=dbusr, password=dbpwd, ssl=dbssl, database='online_ecal_workflows')
        
    def writeData(self) -> None:
        """
        Write data to influxdb.
        """        
        DATA = []
        for i in self.fetchedRuns:
            DATA.append({
                'measurement' : 'run', 
                'tags' : { 'run_number' : i['number'], 'acq_era' : 'test_v1' }, 
                'fields' : { 'new' : True }, 
                'time' : i['startTime']
            })
        self.db.write_points(DATA)
        print("Data Written")

import sys
import enum
import os
from copy import deepcopy as dcopy
from datetime import datetime
from collections import OrderedDict

from influxdb import InfluxDBClient
import urllib3
urllib3.disable_warnings()

from ecalautoctrl.credentials import *

class DatasetCtrl:
    """
    Insert and update in the influxdb data related to the ECAL offline workflows datasets.
    Each file produced by a task has its own entry and is represented with the following table::
    
        {
            'measurement' : 'file',
            'tags' : {
                'workflow'      : 'ECAL offline workflow name',
                'campaign'      : 'Campaign to which the job belongs to',
                'era'           : 'CMS dataset era (can be omitted)',
                'id'            : 'Progressive number starting from 0 that identify the current job within the task',
                'type'          : 'This tag can be used to disentangle files from a job that produces multiple outputs (e.g. ECALElf ntuples friend files)',
                'creation_time' : 'Keep track of original creation time of this file',
                'runs_lumis'    : 'CMSSW style runs and lumisections json string',
                'fills'         : 'List of LHC fills included in the file'
            },
            'time' : 0,
            'fields' : {
                'path'    : 'File location',
                'valid' : 'flag set to true after the integrity of the file has been valided'
            }
        }
    
    """

    def __init__(self, workflow=None, campaign=None, era=None):
        """
        Create a new dataset control instance
        """

        ### allow only pre-determined workflows
        if workflow==None:
            sys.exit('[DatasetCtrl::init] The workflow field is mandatory')

        ### require a campaign
        if campaign==None:
            sys.exit('[DatasetCtrl::init] The campaign field is mandatory')

        ### create point data template
        self.global_data = {
            'measurement' : 'file',
            'tags' : {
                'workflow'      : str(workflow),
                'campaign'      : str(campaign),
                'era'           : str(era),
                'id'            : None,
                'creation_time' : None,
                'type'          : None,
                'runs_lumis'    : None,
                'fills'         : None
            },
            'time' : 0,
            'fields' : {
                'path' : None,
                'valid' : None
            }
        }            

        self.db = InfluxDBClient(host=dbhost, port=dbport, username=dbusr, password=dbpwd, ssl=dbssl, database=dbname)
        
    def insertFile(self, path=None, id=None, type=None, runs_lumis=None, fills=None, valid=None):
        """
        Insert a new file measurement in the influxdb, the functions checks if the same file already exist in the database,
        if so it updates the file record with the new informations.

        :param path: file logical path.
        :type path: str
        :param id: job/file id in the current task.
        :type id: int
        :param type: file type in case job produces multiple output files.
        :type type: str, optional
        :param runs_lumis: CMSSW style runs and lumisections json string
        :type runs_lumis: string, optional
        :param fills: List of LHC fills included in the file (the code will filter duplicates before inserting the new entry in the db).
        :type fills: list, optional
        :param valid: file integrety valid.
        :type valid: bool, optional
        :rtype: bool
        """

        if path==None:
            sys.exit('[DatasetCtrl::insertFile] The path option is mandatory.')        
        if id==None:
            sys.exit('[DatasetCtrl::insertFile] The id option is mandatory.')

        ### get previous entry if it exist for this file
        exist = self.db.query('SELECT last("path"), * FROM "file" WHERE "workflow"=\'%s\' AND "campaign"=\'%s\' AND "era"=\'%s\' AND "type" = \'%s\' AND "id" = \'%s\'' % (self.global_data['tags']['workflow'], self.global_data['tags']['campaign'], self.global_data['tags']['era'], type, str(id)))

        ### prepare new entry with provided data
        data = dcopy(self.global_data)
        data['time'] = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
        data['tags']['runs_lumis'] = runs_lumis
        data['tags']['fills'] = ','.join(set([str(fill) for fill in fills])) if fills else None
        data['fields']['path'] = os.path.abspath(path)

        ### get missing tag and field values from existing latest entry
        if len(exist)>0:
            prev_data = next([v for k,v in exist.items()][0])
            for tag, v in data['tags'].items():                
                if not v and tag in prev_data.keys():
                   data['tags'][tag]  = prev_data[tag]

            if valid:
                data['fields']['valid'] = bool(valid)
            elif 'valid' in prev_data.keys():
                data['fields']['valid'] = bool(prev_data['valid'])

        ### new file, add creation_time
        else:
            data['tags']['id'] = str(id)
            data['tags']['type'] = str(type)
            data['tags']['creation_time'] = data['time']

        return self.db.write_points([data])

    @staticmethod
    def listDatasets():
        """
        Get all available datasets recorded in the db

        :rtype: list
        """

        db = InfluxDBClient(host=dbhost, port=dbport, username=dbusr, password=dbpwd, ssl=dbssl, database=dbname)
        files_query = db.query('SELECT last("path"), "campaign", "era", "workflow" FROM "file" GROUP BY "campaign", "era", "workflow"')

        print(files_query.items())
        
    def getAllFiles(self):
        """
        Get all the files produced by the current task.

        :rtype: list
        """

        files_query = self.db.query('SELECT last("path"), "type", "path" FROM "file" WHERE "workflow"=\'%s\' AND "campaign"=\'%s\' AND "era"=\'%s\' GROUP BY "id", "type"' % 
                                    (self.global_data['tags']['workflow'], self.global_data['tags']['campaign'], self.global_data['tags']['era']))

        files = []
        for key, data in files_query.items():
            for afile in data:
                files.append({
                    'type' : afile['type'],
                    'path' : afile['path']
                })

        return files

    def getFilesIdRange(self, start_id=0, n_files=None, type=None, data=['path']):
        """
        Get all files with Id in the [start_id, start_id+n_files] range.

        :param start_id: first id.
        :type start_id: int.
        :param n_files: number of files to be returned (default all).
        :type n_files: int, optional.
        :param type: file type in case each job produced more than one output.
        :type type: str, optinal.
        :rtype: list
        """

        files_query = self.db.query('SELECT last("path"),%s FROM "file" WHERE "workflow"=\'%s\' AND "campaign"=\'%s\' AND "era"=\'%s\' AND "type" = \'%s\' GROUP BY "id"' % ('"'+'", "'.join(data)+'"', self.global_data['tags']['workflow'], self.global_data['tags']['campaign'], self.global_data['tags']['era'], type))

        sorted_files = {int(key[1]['id']): data for key, data in files_query.items()}
        sorted_files = OrderedDict(sorted(sorted_files.items()))

        n_files = n_files if n_files != None else len(sorted_files)
        
        files = []     
        for key, values in sorted_files.items():
            if key >= int(start_id) and len(files) < int(n_files):
                for afile in values:
                    files.append({fld: afile[fld] for fld in data})        
                    
        return files
        

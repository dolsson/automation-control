.. ECAL automation control documentation master file, created by
   sphinx-quickstart on Fri Mar 12 10:19:56 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ECAL automation control's documentation!
===================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Installation
============

The package can be installed through pip:

.. code-block:: bash

   pip3 install 'https://gitlab.cern.ch/cms-ecal-dpg/ECALELFS/automation-control.git'

Note that in most cases the :code:`--user` option is needed in order to install without having an admin accout (i.e. on lxplus). The pip installation works also
for CMSSW environment after the :code:`cmsenv` has been setup and by adding the :code:`-I --prefix=$CMSSW_BASE/external/$SCRAM_ARCH/`.

On lxplus the module is pre-installed in the ECAL software environment, so you just need to activate the environment with

.. code-block:: bash

   source /eos/project-c/cms-ecal-calibration/ecal-venv/bin/activate

ECAL ecalautomation.py script
=============================
.. argparse::
   :filename: ../bin/ecalautomation.py
   :func: cmdline_options
   :prog: ecalautomation.py

ECAL ecalrunctrl.py script
=============================
.. argparse::
   :filename: ../bin/ecalrunctrl.py
   :func: cmdline_options
   :prog: ecalrunctrl.py
          
ECAL OnlineTools
================
.. autoclass:: ecalautoctrl.QueryRunRegistry
   :members:

.. autoclass:: ecalautoctrl.QueryDBS
   :members:

.. autoclass:: ecalautoctrl.RunCtrl
   :members:      
      
ECAL JobCtrl
============
.. autoclass:: ecalautoctrl.JobCtrl
   :members:

.. autoclass:: ecalautoctrl.JobStatus
   :members:
      
ECAL DatasetCtrl
================
.. autoclass:: ecalautoctrl.DatasetCtrl
   :members:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

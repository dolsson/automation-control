#!/usr/bin/env python3

import subprocess
from pathlib import Path
from datetime import datetime

from influxdb import InfluxDBClient
import urllib3
urllib3.disable_warnings()

from ecalautoctrl.credentials import *

def main():
    """
    Get DPG's EOS area usage.
    Note: this script does not even try to be generic, it is tailored to the dpg_ecal EOS area.
    Sizes: sizes are in Tb using eos convention 1 Tb = 1e12 bytes (note that du instead counts in multiples of 1024)
    """
    
    alca_ecalcalib = subprocess.run(['eos', 'root://eoscms.cern.ch', 'ls', '-l', '/eos/cms/store/group/dpg_ecal/alca_ecalcalib/'], capture_output=True)
    comm_ecal = subprocess.run(['eos', 'root://eoscms.cern.ch', 'ls', '-l', '/eos/cms/store/group/dpg_ecal/comm_ecal/'], capture_output=True)

    data = []
    tot_alca = 0.
    for dir in alca_ecalcalib.stdout.splitlines():        
        dirinfo = dir.decode().split()
        data.append({
            'measurement' : 'eos_quota',
            'time' : datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ'),
            'tags' : {
                'dirname' : dirinfo[-1]},
            'fields' : {
                'size' : float(dirinfo[4])/(1e12),
                'path' : '/eos/cms/store/group/dpg_ecal/alca_ecalcalib/'+dirinfo[-1]}})
        tot_alca += float(dirinfo[4])/(1e12)
    tot_comm = 0.
    for dir in comm_ecal.stdout.splitlines():        
        dirinfo = dir.decode().split()
        data.append({
            'measurement' : 'eos_quota',
            'time' : datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ'),
            'tags' : {
                'dirname' : dirinfo[-1]},
            'fields' : {
                'size' : float(dirinfo[4])/(1e12),
                'path' : '/eos/cms/store/group/dpg_ecal/comm_ecal/'+dirinfo[-1]}})
        tot_comm += float(dirinfo[4])/(1e12)

    ### Total 
    data.extend([{
        'measurement' : 'eos_quota',
        'time' : datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ'),
        'tags' : {
            'dirname' : 'alca_ecalcalib'},
        'fields' : {
            'size' : tot_alca,
            'path' : '/eos/cms/store/group/dpg_ecal/alca_ecalcalib/'}},
                 {
        'measurement' : 'eos_quota',
        'time' : datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ'),
        'tags' : {
            'dirname' : 'comm_ecal'},
        'fields' : {
            'size' : tot_comm,
            'path' : '/eos/cms/store/group/dpg_ecal/comm_ecal/'}},
                 {
        'measurement' : 'eos_quota',
        'time' : datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ'),
        'tags' : {
            'dirname' : 'dpg_ecal'},
        'fields' : {
            'size' : tot_alca+tot_comm,
            'path' : '/eos/cms/store/group/dpg_ecal/'}}])

    ### Write to the db
    db = InfluxDBClient(host=dbhost, port=dbport, username=dbusr, password=dbpwd, ssl=dbssl, database=dbname)
    db.write_points(data)

if __name__ == '__main__':
    main()

from OnlineTools import*

db = InfluxDBClient(host=dbhost, port=dbport, username=dbusr, password=dbpwd, ssl=dbssl, database=dbname)

dbQuery = db.query('SELECT run_number,last("new") FROM "run"')

lastRunNumber = 320000

for id,i in dbQuery.items():
    lastRunNumber = next(i)['run_number']

runQuery = QueryRunRegistry(int(lastRunNumber),int(lastRunNumber)+20)
test = runQuery.get_runs()

DBSQuery = QueryDBS(test)
test2 = DBSQuery.fetchFiles()

write = DatabaseWrite(test)
write.writeData()
print('Done')

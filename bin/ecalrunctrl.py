#!/usr/bin/env python3

import argparse
import logging
import sys
from datetime import datetime
from ecalautoctrl import QueryRunRegistry, RunCtrl

logging.basicConfig(stream=sys.stdout, level=logging.INFO)

def cmdline_options():
    """
    Return the argparse instance to handle the cmdline options for this script (needed to easily generate 
    the docs using sphinx-argparse)
    """

    parser = argparse.ArgumentParser(description=
    """
    Fetch information from runregistry and create a corresponding entry
    in the ECAL influxdb
    """, formatter_class=argparse.RawTextHelpFormatter)

    ### sub commands
    subparsers = parser.add_subparsers(dest='subcommand', description='Select operation')

    ### reply subcommand
    replay_parser = subparsers.add_parser('replay', help='Inject a specific list of runs in "new" state')
    # options to specify which runs to be re-inserted in the ECAL db (run-based or time-based)
    runlist_opt = replay_parser.add_mutually_exclusive_group(required=True)
    define_run_range = lambda opt : opt.split(',') if len(opt.split(','))==2 else [opt, opt]
    runlist_opt.add_argument('--run',
                             dest='range',
                             default='0',
                             type=define_run_range,
                             help='Inject this run')
    runlist_opt.add_argument('--range',
                             dest='range',
                             default='0,0',
                             type=define_run_range,
                             help='First and last run to be considered, comma separated')
    replay_parser.add_argument('--date-begin',
                               dest='date_begin',
                               default='30/03/2010',
                               type=lambda opt : datetime.strptime(opt, "%d/%m/%Y"),
                               help='Start date. format: dd/mm/yyyy')
    replay_parser.add_argument('--date-end',
                               dest='date_end',
                               default='30/03/2050',
                               type=lambda opt : datetime.strptime(opt, "%d/%m/%Y"),
                               help='End date. format: dd/mm/yyyy')

    ### sync subcommand
    sync_parser = subparsers.add_parser('sync', help='Inject runs acquired since last inserted run')
    mode_opt = sync_parser.add_mutually_exclusive_group(required=False)
    mode_opt.add_argument('--all',
                          dest='all',
                          default=True,
                          action='store_true',
                          help='Fetch all new runs')
    mode_opt.add_argument('--single',
                          dest='all',
                          default=True,
                          action='store_false',
                          help='Fetch only one new run (mainly for debug)')

    return parser

if __name__ == '__main__':

    opts = cmdline_options().parse_args()
    
    rr = QueryRunRegistry()
    runctrl = RunCtrl(acq_era='test')

    n_runs = -1
    if opts.subcommand == 'sync':
        opts.range = [runctrl.getLastRun()+1, None]
        n_runs = -1 if opts.all else 1

    # get runs from runregistry
    runs = rr.get_runs(min_run=opts.range[0], max_run=opts.range[1])
    runs.sort(key=lambda r : r['run_number'])
    # limit number of runs
    runs = runs[:n_runs]

    # insert the run as new in all active workflows
    status = {}
    for wf in runctrl.getWorkflows()['active']:
        status[wf+'-new'] = True
    
    if runctrl.updateStatus([r['run_number'] for r in runs], status=status):
        logging.info(f'Runs {",".join([str(r["run_number"]) for r in runs])} injected as new in ECAL db')
        


